# Setup

Start a container with a test database

Login with you gitlab credentials with `sudo docker login registry.gitlab.com`

```
sudo docker run --publish=27017:27017 --name dbms -d mongo

sudo docker pull registry.gitlab.com/cphbussiness-test-19/dummy-data:latest 
sudo docker run --link dbms:mongo registry.gitlab.com/cphbussiness-test-19/dummy-data:latest 

sudo docker pull registry.gitlab.com/cphbussiness-test-19/authservice:latest
sudo docker run -d -p 32001:32001 -e jwtSecret=superhemmligkey1234 -e port=32001 -e databaseurl="mongodb://mongo/les" \
    --link dbms:mongo --name authservice registry.gitlab.com/cphbussiness-test-19/authservice:latest 
    

sudo docker pull registry.gitlab.com/cphbussiness-test-19/dataservice:latest
sudo docker run -d -p 32002:80 -e DATABASEURL="mongodb://mongo/les" --link dbms:mongo --name dataservice registry.gitlab.com/cphbussiness-test-19/dataservice:latest

```
