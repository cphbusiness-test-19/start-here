# Lyngby Evening School - IT system 

Dette er besvarelsen til Test-semester-projekt-opgaven.

Projektet er lavet af:<br />
Yosuke Ueda (cph-yu173)<br />
Alexander Agregaard Nielsen (cph-an178)<br />
Benjamin Schultz Larsen (cph-bl135)<br />
Jacob Sørensen (cph-js284)<br />
<br />
<b>CASE-Beskrivelse</b><br />
Hele case beskrivelsen kan [læses her](https://github.com/datsoftlyngby/soft2019spring-test/blob/master/Assignments/TestSemesterProject.pdf)

## Forord
Denne besvarelse indeholder ikke en rapport i traditionel forstand - i stedet lægges der op til at læseren selv går opdagelse i de mange artefakter(<i>se oversigt nedenfor</i>) som tilsammen udgør vores besvarelse. For en evaluering af projektet se venligst [artefakt evaluering](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20Evaluering%20Reflektering.pdf) 

## Produktet
Det færdige produkt kan ses på [IP Address: 138.68.102.17](http://138.68.102.17) <br />
Adgang til administration-modulet (Rektor for skolen/ medarbejder) kan opnås ved <br />
<br />
Brugernavn : <b>Guzman</b><br />
Password : <b>test</b><br />
<br />
Hvis koden skal hentes og afprøves lokalt anbefaler vi at følge denne 
[vejledningen](https://gitlab.com/cphbusiness-test-19/start-here/blob/master/Setup.md)  

## Screenshots
Acceptkriterierne for vores userstories dokumenteres i en serie screenshots, disse kan findes i projektet “ProjectPipeline” i mappen  [Artifacts](https://gitlab.com/cphbusiness-test-19/projectpipeline/-/jobs/artifacts/master/browse?job=integrationtest) - efter CI/CD pipeline har kørt




### Alle vores artefakter

- [Video af pipeline-youtube](https://www.youtube.com/watch?v=7jp5Yoa1Rlg)
- [Workflow](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20arbejdsflow.pdf)
- [Feature list](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20Featureliste%20.pdf)
- [JMeter](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20Jmeter.pdf)
- [Scenarios](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20Scenarios.pdf)
- [Test techniques](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20test%20teknikker.pdf)
- [Test categories](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20Testkategorier.pdf)
- [User stories vote](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20Userstories%20Afstemning.pdf)
- [User stories authentication](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20userstories%20Authentication.pdf)
- [User stories CRUD courses](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20Userstories%20CRUDkurser.pdf)
- [Code coverage](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Code%20Coverage.pdf)
- [Pipelines](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Pipelines.png)
- [Test artefact tables End 2 End](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Test%20Artefakt%20tabeler%20-%20End%202%20End.pdf)
- [Test artefact tables Integration test](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Test%20Artefakt%20tabeler%20-%20Integration%20test.pdf)
- [Test artefact tables Unittest](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Test%20Artefakt%20tabeler%20-%20Unittest.pdf)
- [Unitmock](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/unitmock.png)
- [Evaluering](https://gitlab.com/cphbusiness-test-19/artifacts/blob/master/Artefakt%20Evaluering%20Reflektering.pdf) 

### Alle vores projekt repos
- [AdministrativeModule](https://gitlab.com/cphbusiness-test-19/administrativemodule)
- [Artifacts](https://gitlab.com/cphbusiness-test-19/artifacts)
- [AuthService](https://gitlab.com/cphbusiness-test-19/authservice)
- [DataService](https://gitlab.com/cphbusiness-test-19/dataservice)
- [Dummy Data ](https://gitlab.com/cphbusiness-test-19/dummy-data)
- [ProjectPipeline](https://gitlab.com/cphbusiness-test-19/projectpipeline)
- [PublicCatalog](https://gitlab.com/cphbusiness-test-19/publiccatalog)